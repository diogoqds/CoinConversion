# frozen_string_literal: true

RSpec.configure do |config|
  config.before(:each) do
    stub_request(:get, /currencydatafeed.com/)
      .with(headers: {
              "Accept" => "*/*"
            }).to_return(status: 200, body: '
      {
        "status": true,
        "currency": [
            {
                "currency": "USD/BRL",
                "value": "3.41325",
                "date": "2018-04-20 17:22:59",
                "type": "original"
            }
        ]
      }', headers: {})

      stub_request(:get, /api.coindesk.com/)
      .with(headers: {
          "Accept" => "*/*"
        }).to_return(status: 200, body: '{
        "time": {
          "updated": "Dec 27, 2018 11:30:00 UTC",
          "updatedISO": "2018-12-27T11:30:00+00:00",
          "updateduk": "Dec 27, 2018 at 11:30 GMT"
        },
        "disclaimer": "This data was produced from the CoinDesk Bitcoin Price Index (USD). Non-USD currency data converted using hourly conversion rate from openexchangerates.org",
        "bpi": {
          "USD": {
            "code": "USD",
            "rate": "3,779.9300",
            "description": "United States Dollar",
            "rate_float": 3779.93
          },
          "EUR": {
            "code": "EUR",
            "rate": "3,314.8021",
            "description": "Euro",
            "rate_float": 3314.8021
          }
        }
      }', headers: {})

      stub_request(:get, /blockchain.info/)
      .with(headers: {
          "Accept" => "*/*"
        }).to_return(status: 200, body: '
          1.00150436
        ', headers: {})
  end
end
