# frozen_string_literal: true

require "rest-client"
require "json"

class BitcoinService
  def initialize(source_currency, target_currency, amount)
    @source_currency = source_currency
    @target_currency = target_currency
    @amount = amount.to_f
  end

  def perform
    value = if @source_currency == "BT"
              conversion_to_real_money
            else
              conversion_to_bitcoin
            end
    value * @amount
  rescue RestClient::ExceptionWithResponse => e
    e.response
  end

  private

  def conversion_to_real_money
    url = "https://api.coindesk.com/v1/bpi/currentprice/#{@target_currency}.json"
    value = RestClient.get url
    value = JSON.parse(value)["bpi"][@target_currency]["rate_float"]
    value.to_f
  end

  def conversion_to_bitcoin
    url = "https://blockchain.info/tobtc?currency=#{@source_currency}&value=#{@amount}"
    value = RestClient.get url
    value.to_f
  end
end
