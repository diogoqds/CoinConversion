# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Exchange Currency Process", type: :system, js: true do
  describe "when choose real money" do
    it "show exchanged value" do
      visit "/"
      within "#exchange_form" do
        select("EUR", from: "source_currency")
        select("BRL", from: "target_currency")
        fill_in("amount", with: 10)
      end

      expect(page).to have_selector("#result")
    end
  end

  describe "when use bitcon" do
    it "show EUR conversion" do
      visit "/"
      within "#exchange_form" do
        select("BT", from: "source_currency")
        select("EUR", from: "source_currency")
        fill_in("amount", with: 1)
      end

      expect(page).to have_selector("#result")
    end

    it "show BT conversion" do
      visit "/"
      within "#exchange_form" do
        select("BRL", from: "source_currency")
        select("BT", from: "source_currency")
        fill_in("amount", with: 1)
      end

      expect(page).to have_selector("#result")
    end
  end
end
