# frozen_string_literal: true

require "spec_helper"
require "json"
require "./app/services/bitcoin_service"

describe "Bitcoin Currency" do
  it "exchange to real money" do
    amount = rand(0..9999)
    res = BitcoinService.new("BT", "EUR", amount).perform
    expect(res.is_a?(Numeric)).to eql(true)
    expect(res != 0 || amount == 0).to eql(true)
  end

  it "exchange to bitcoin" do
    amount = rand(0..9999)
    res = BitcoinService.new("EUR", "BT", amount).perform
    expect(res.is_a?(Numeric)).to eql(true)
    expect(res != 0 || amount == 0).to eql(true)
  end
end
