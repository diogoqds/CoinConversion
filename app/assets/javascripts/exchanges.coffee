# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).ready ->
  $('form').submit ->
    if $('form').attr('action') == '/convert'
        convertCoin()
      return false
  $('#reverse-button').click ->
    source_currency = $('#source_currency')
    source_currency_aux = source_currency.val()

    target_currency = $('#target_currency')
    target_currency_aux = target_currency.val()

    source_currency.val(target_currency_aux)
    target_currency.val(source_currency_aux)
    convertCoin()

  $('#amount').on 'input', (event) ->
    convertCoin()
    return

  $('#source_currency').change ->
    convertCoin()
    return

  $('#target_currency').change ->
    convertCoin()
    return
  convertCoin = () ->
    $.ajax '/convert',
      type: 'GET',
      dataType: 'json',
      data: {
          source_currency:  $('#source_currency').val(),
          target_currency:  $('#target_currency').val(),
          amount:           $('#amount').val()
      }
      error: (jqXHR, textStatus, errorThrown) ->
        alert textStatus
      success: (data, text, jqXHR) ->
        $('#result').val(data.value)
