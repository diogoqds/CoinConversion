# frozen_string_literal: true

class CurrencyAuxService
  attr_reader :value
  def initialize(source_currency, target_currency, amount)
    @value = if source_currency == "BT" || target_currency == "BT"
               BitcoinService.new(source_currency, target_currency, amount).perform
             else
               ExchangeService.new(source_currency, target_currency, amount).perform
             end
  end
end
