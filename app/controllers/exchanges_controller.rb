# frozen_string_literal: true

class ExchangesController < ApplicationController
  def index; end

  def convert
    value = CurrencyAuxService.new(params[:source_currency], params[:target_currency], params[:amount]).value
    render json: {value: value}
  end
end
